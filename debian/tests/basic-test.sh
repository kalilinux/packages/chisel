#!/bin/sh

set -eu

# With no argument, chisel outputs a help message on stdout,
# and it returns zero. Let's check.
chisel

# Get version, make sure it's the version of the package.
package_version=$(dpkg-query -W | grep ^chisel | awk '{print $2}')
program_version=$(chisel | grep Version: | awk '{print $2}')
if [ "$program_version" != "$package_version" ]; then
	echo "Version mismatch: package='$package_version', program='$program_version'" >&2
	exit 1
fi
